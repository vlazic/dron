export enum Field {
  GROUND,
  WALL,
  BASE,
  LIGHT,
  COVERED,
}
