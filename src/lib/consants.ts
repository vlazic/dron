import { Field } from "../types/field";

export const FieldColors: { [colorId in Field]: string } = {
  [Field.GROUND]: "#00ff00",
  [Field.WALL]: "#cc0000",
  [Field.BASE]: "#ffffff",
  [Field.LIGHT]: "#7fa832",
  [Field.COVERED]: "#08a34b",
};
