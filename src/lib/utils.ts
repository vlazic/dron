function arrayCountTrue(sum: number, element: boolean[]): number {
  sum += element.filter(Boolean).length;
  return sum;
}

export { arrayCountTrue };
